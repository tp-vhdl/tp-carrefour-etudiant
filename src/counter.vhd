-------------------------------------------------------------------------------
-- Title      : counter
-- Project    :
-------------------------------------------------------------------------------
-- File       : counter.vhd
-- Author     : Jean-Noel BAZIN  <jnbazin@pc-disi-026.enst-bretagne.fr>
-- Company    :
-- Created    : 2018-05-30
-- Last update: 2024-03-27
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Programable counter used to count second before changing colors
-- on the traffic light. one counter generating a pulse for each second from
-- clock, a second counter to decount the number of second
-------------------------------------------------------------------------------
-- Copyright (c) 2018
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-05-30  1.0      jnbazin Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter is

    generic (
        G_number_of_cycle_per_second : integer := 1     -- number of clock cycle in one second, must be at least 1
        );
    port (
        I_clk      : in  std_logic;                     -- Global input clock
        I_init     : in  std_logic;                     -- Synchronous initialization
        I_enable   : in  std_logic;                     -- Synchronous chip enable control s
        I_maxCount : in  std_logic_vector(3 downto 0);  -- max value for the counter
        O_dataOut  : out std_logic_vector(3 downto 0);  -- counter output value
        O_done     : out std_logic
        );
end entity counter;

architecture arch_counter of counter is

    constant G_clockPeriodToCount : integer := G_number_of_cycle_per_second-1;

    signal SR_counterPulse  : integer range 0 to G_clockPeriodToCount;  -- Counter internal value
    signal SC_secondPulse   : std_logic;                                -- signal raised for one clock period every second
    signal SR_counterSecond : integer range 0 to 9;                     -- Counter internal value signal

begin

-- purpose: Generate a pulse every second according to the clock period
-- type   : sequential
-- inputs : I_clk, I_init, I_enable
-- output : SR_counterPulse
    pulse_counter : process (I_clk) is
    begin

        __BLANK_TO_FILL__

    end process pulse_counter;

    SC_secondPulse <= __BLANK_TO_FILL__



-- purpose: Drive the SR_counter signal to count according to I_init and I_incr
-- type   : sequential
-- inputs : I_clk, I_init, I_enable, I_maxCount
-- outputs: SR_counterSecond
    second_counter : process (I_clk) is
    begin

        __BLANK_TO_FILL__

    end process second_counter;

    O_dataOut <= std_logic_vector(to_unsigned(SR_counterSecond, 4));
    O_done    <= '1' when(SR_counterSecond = 0 and SC_secondPulse = '1') else '0';

end architecture arch_counter;
