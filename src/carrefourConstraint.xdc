###################################################################################
## Horloge 100MHz
###################################################################################
##Bank = 35, Pin name = IO_L12P_T1_MRCC_35,					Sch name = CLK100MHZ
set_property PACKAGE_PIN E3 [get_ports I_clk]							
	set_property IOSTANDARD LVCMOS33 [get_ports I_clk]
	create_clock -add -name I_clk -period 10.00 -waveform {0 5} [get_ports I_clk]

###################################################################################
## Bouton Reset
###################################################################################
##Bank = 15, Pin name = IO_L3P_T0_DQS_AD1P_15,				Sch name = CPU_RESET
set_property PACKAGE_PIN F15 [get_ports I_rst]				
	set_property IOSTANDARD LVCMOS33 [get_ports I_rst]


###################################################################################
## Bouton presence chemin
###################################################################################
set_property PACKAGE_PIN E16 [get_ports I_presence]						
    set_property IOSTANDARD LVCMOS33 [get_ports I_presence]



###################################################################################
## Afficheurs 7 segments
###################################################################################
##7 segment display
##Bank = 34, Pin name = IO_L2N_T0_34,						Sch name = CA
set_property PACKAGE_PIN L3 [get_ports {O_7segment[6]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segment[6]}]
##Bank = 34, Pin name = IO_L3N_T0_DQS_34,					Sch name = CB
set_property PACKAGE_PIN N1 [get_ports {O_7segment[5]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segment[5]}]
##Bank = 34, Pin name = IO_L6N_T0_VREF_34,					Sch name = CC
set_property PACKAGE_PIN L5 [get_ports {O_7segment[4]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segment[4]}]
##Bank = 34, Pin name = IO_L5N_T0_34,						Sch name = CD
set_property PACKAGE_PIN L4 [get_ports {O_7segment[3]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segment[3]}]
##Bank = 34, Pin name = IO_L2P_T0_34,						Sch name = CE
set_property PACKAGE_PIN K3 [get_ports {O_7segment[2]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segment[2]}]
##Bank = 34, Pin name = IO_L4N_T0_34,						Sch name = CF
set_property PACKAGE_PIN M2 [get_ports {O_7segment[1]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segment[1]}]
##Bank = 34, Pin name = IO_L6P_T0_34,						Sch name = CG
set_property PACKAGE_PIN L6 [get_ports {O_7segment[0]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segment[0]}]

##Bank = 34, Pin name = IO_L16P_T2_34,						Sch name = DP
#set_property PACKAGE_PIN M4 [get_ports dp]							
	#set_property IOSTANDARD LVCMOS33 [get_ports dp]

##Bank = 34, Pin name = IO_L18N_T2_34,						Sch name = AN0
set_property PACKAGE_PIN N6 [get_ports {O_7segmentSelect[0]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[0]}]
##Bank = 34, Pin name = IO_L18P_T2_34,						Sch name = AN1
set_property PACKAGE_PIN M6 [get_ports {O_7segmentSelect[1]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[1]}]
##Bank = 34, Pin name = IO_L4P_T0_34,						Sch name = AN2
set_property PACKAGE_PIN M3 [get_ports {O_7segmentSelect[2]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[2]}]
##Bank = 34, Pin name = IO_L13_T2_MRCC_34,					Sch name = AN3
set_property PACKAGE_PIN N5 [get_ports {O_7segmentSelect[3]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[3]}]
##Bank = 34, Pin name = IO_L3P_T0_DQS_34,					Sch name = AN4
set_property PACKAGE_PIN N2 [get_ports {O_7segmentSelect[4]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[4]}]
##Bank = 34, Pin name = IO_L16N_T2_34,						Sch name = AN5
set_property PACKAGE_PIN N4 [get_ports {O_7segmentSelect[5]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[5]}]
##Bank = 34, Pin name = IO_L1P_T0_34,						Sch name = AN6
set_property PACKAGE_PIN L1 [get_ports {O_7segmentSelect[6]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[6]}]
##Bank = 34, Pin name = IO_L1N_T034,							Sch name = AN7
set_property PACKAGE_PIN M1 [get_ports {O_7segmentSelect[7]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[7]}]





###################################################################################
## Feux tricolores : Led vector RV & RO & RR & CV & CO & CR
###################################################################################
##Pmod Header O_led
##Bank = 15, Pin name = IO_L1N_T0_AD0N_15,					Sch name = O_led1
set_property PACKAGE_PIN B13 [get_ports {O_led[5]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_led[5]}]
##Bank = 15, Pin name = IO_L5N_T0_AD9N_15,					Sch name = O_led2
set_property PACKAGE_PIN F14 [get_ports {O_led[4]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_led[4]}]
##Bank = 15, Pin name = IO_L16N_T2_A27_15,					Sch name = O_led3
set_property PACKAGE_PIN D17 [get_ports {O_led[3]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_led[3]}]
##Bank = 15, Pin name = IO_L16P_T2_A28_15,					Sch name = O_led4
#set_property PACKAGE_PIN E17 [get_ports {O_led[]}]					
#	set_property IOSTANDARD LVCMOS33 [get_ports {O_led[]}]
##Bank = 15, Pin name = IO_0_15,							Sch name = O_led7
set_property PACKAGE_PIN G13 [get_ports {O_led[2]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_led[2]}]
##Bank = 15, Pin name = IO_L20N_T3_A19_15,					Sch name = O_led8
set_property PACKAGE_PIN C17 [get_ports {O_led[1]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_led[1]}]
#Bank = 15, Pin name = IO_L21N_T3_A17_15,					Sch name = O_led9
set_property PACKAGE_PIN D18 [get_ports {O_led[0]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {O_led[0]}]
##Bank = 15, Pin name = IO_L21P_T3_DQS_15,					Sch name = O_led10
#set_property PACKAGE_PIN E18 [get_ports {O_led[]}]					
	#set_property IOSTANDARD LVCMOS33 [get_ports {O_led[]}]