-------------------------------------------------------------------------------
-- Title      : Testbench for design "carrefourUnit"
-- Project    :
-------------------------------------------------------------------------------
-- File       : carrefourUnit_tb.vhd
-- Author     : Jean-Noel BAZIN  <jnbazin@pc-disi-026.enst-bretagne.fr>
-- Company    :
-- Created    : 2018-05-30
-- Last update: 2024-03-27
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2018
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-05-30  1.0      jnbazin Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-------------------------------------------------------------------------------

entity tb_carrefourUnit is

end entity tb_carrefourUnit;

-------------------------------------------------------------------------------

architecture archi_tb_carrefourUnit of tb_carrefourUnit is
    component carrefourUnit is
        generic (
            G_number_of_cycle_per_second : integer
            );
        port (
            I_clk      : in  std_logic;
            I_rst      : in  std_logic;
            I_presence : in  std_logic;
            O_led      : out std_logic_vector(5 downto 0);
            O_dataOut  : out std_logic_vector(3 downto 0));
    end component carrefourUnit;

    signal I_clk      : std_logic := '1';
    signal I_rst      : std_logic;
    signal I_presence : std_logic;
    signal O_led      : std_logic_vector(5 downto 0);
    signal O_dataOut  : std_logic_vector(3 downto 0);


begin  -- architecture archi_tb_carrefourUnit

    -- component instantiation
    DUT : carrefourUnit
        generic map (
            G_number_of_cycle_per_second => 10  -- number of clock cycles in one second, small number for test, 50 000 000 for 50MHz clock onboard
            )
        port map (
            I_clk      => I_clk,
            I_rst      => I_rst,
            I_presence => I_presence,
            O_led      => O_led,
            O_dataOut  => O_dataOut);

    -- clock generation
    I_clk      <= not I_clk after 7 ns;
    I_rst      <= '0', '1'  after 13 ns, '0' after 31 ns;
    I_presence <= '0', '1'  after 823 ns, '0' after 848 ns, '1' after 2211 ns, '0' after 3800 ns;

end architecture archi_tb_carrefourUnit;
