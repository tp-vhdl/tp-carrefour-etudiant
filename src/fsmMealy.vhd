-------------------------------------------------------------------------------
-- Title      : fsmMealy
-- Project    :
-------------------------------------------------------------------------------
-- File       : fsmMealy.vhd
-- Author     : Jean-Noel BAZIN  <jnbazin@pc-disi-026.enst-bretagne.fr>
-- Company    :
-- Created    : 2018-05-30
-- Last update: 2024-03-27
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Mealy type finite state machine. Controler of the traffic light
-- circuit
-------------------------------------------------------------------------------
-- Copyright (c) 2018
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-05-30  1.0      jnbazin Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fsmMealy is
    port (
        I_clk         : in  std_logic;                     -- Global clock
        I_rst         : in  std_logic;                     -- Global asynchronous active high reset
        I_timerDone   : in  std_logic;                     -- Indicates the timer is done
        I_presence    : in  std_logic;                     -- A vehicule is present on the track
        O_led         : out std_logic_vector(5 downto 0);  -- Drives the leds of the traffic light. Signal format : RV & RO & RR & CV & CO & CR
        O_maxCount    : out std_logic_vector(3 downto 0);  -- Indicates to the timer the time to count
        O_initTimer   : out std_logic;                     -- Initializes the timer
        O_enableTimer : out std_logic                      -- Enables timer processing
        );
end entity fsmMealy;

architecture archi_fsmMealy of fsmMealy is

    constant CST_TimerRV   : integer := 9;  -- Number of seconds the road light should remain green
    constant CST_TimerROCO : integer := 3;  -- Number of seconds the road light or the path remains orange
    constant CST_TimerCV   : integer := 7;  -- Number of seconds the path light remains green
    constant CST_TimerSECU : integer := 2;  -- Number of seconds the road and the path lights remain red together

    constant CST_RRCR : std_logic_vector(5 downto 0) := "001001";
    constant CST_ROCR : std_logic_vector(5 downto 0) := "010001";
    constant CST_RVCR : std_logic_vector(5 downto 0) := "100001";
    constant CST_RRCV : std_logic_vector(5 downto 0) := "001100";
    constant CST_RRCO : std_logic_vector(5 downto 0) := "001010";

    type T_state is (INIT, RRCR1, RVCR1, RVCR2, ROCR, RRCR2, RRCV, RRCO);  -- List of states
    signal SR_presentState : T_state;                                      -- Signal for the present state (provided by the state register)
    signal SC_futurState   : T_state;                                      -- Result of the evaluation of the futur state
    signal SC_maxCountInt  : integer range 0 to 9;                         -- Signal indicating the max value for second counter

begin

    -- State register
    process (I_clk) is
    begin
        if rising_edge(I_clk) then
            if I_rst = '1' then
                SR_presentState <= INIT;
            else
                SR_presentState <= SC_futurState;
            end if;
        end if;
    end process;


    process (SR_presentState, I_timerDone, I_presence) is
    begin
        O_initTimer    <= '0';
        O_enableTimer  <= '0';
        SC_maxCountInt <= CST_TimerSECU;
        O_led          <= CST_RRCR;     -- road is red and track is red
        case SR_presentState is

            when INIT =>
                __BLANK_TO_FILL__

            __BLANK_TO_FILL__

        end case;
    end process;

    O_maxCount <= __BLANK_TO_FILL__


end architecture archi_fsmMealy;
