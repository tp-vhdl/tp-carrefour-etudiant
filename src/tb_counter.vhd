-------------------------------------------------------------------------------
-- Title      : tb_counter
-- Project    :
-------------------------------------------------------------------------------
-- File       : tb_counter.vhd
-- Author     : Jean-Noel BAZIN  <jnbazin@pc-disi-026.enst-bretagne.fr>
-- Company    :
-- Created    : 2018-05-30
-- Last update: 2024-03-27
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Test bench for the counter
-------------------------------------------------------------------------------
-- Copyright (c) 2018
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-05-30  1.0      jnbazin Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_counter is
end entity tb_counter;

architecture archi_tb_counter of tb_counter is

    component counter is
        generic (
            G_number_of_cycle_per_second : integer := 1
            );
        port (
            I_clk      : in  std_logic;
            I_init     : in  std_logic;
            I_enable   : in  std_logic;
            I_maxCount : in  std_logic_vector(3 downto 0);
            O_dataOut  : out std_logic_vector(3 downto 0);
            O_done     : out std_logic);
    end component counter;

    signal I_clk      : std_logic := '0';
    signal I_init     : std_logic;
    signal I_enable   : std_logic;
    signal I_maxCount : std_logic_vector(3 downto 0);
    signal O_dataOut  : std_logic_vector(3 downto 0);
    signal O_done     : std_logic;


begin  -- architecture archi_tb_counter

    I_clk      <= not I_clk after 7 ns;
    I_init     <= '0', '1'  after 66 ns, '0' after 89 ns, '1' after 347 ns, '0' after 383 ns;
    I_enable   <= '0', '1'  after 13 ns, '0' after 39 ns, '1' after 101 ns, '0' after 2589 ns;
    I_maxCount <= std_logic_vector(to_unsigned(9, 4));

    counter_1 : counter
        generic map (
            G_number_of_cycle_per_second => 7
            )
        port map (
            I_clk      => I_clk,
            I_init     => I_init,
            I_enable   => I_enable,
            I_maxCount => I_maxCount,
            O_dataOut  => O_dataOut,
            O_done     => O_done);

end architecture archi_tb_counter;
