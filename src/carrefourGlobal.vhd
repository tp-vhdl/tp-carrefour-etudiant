-------------------------------------------------------------------------------
-- Title      : carrefourGlobal
-- Project    :
-------------------------------------------------------------------------------
-- File       : carrefourGlobal.vhd
-- Author     : Jean-Noel BAZIN  <jnbazin@pc-disi-026.enst-bretagne.fr>
-- Company    :
-- Created    : 2018-06-05
-- Last update: 2024-03-27
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: VHDL module including carrefourUnit and 7 segment coding module
-------------------------------------------------------------------------------
-- Copyright (c) 2018
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-06-05  1.0      jnbazin Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity carrefourGlobal is
    generic (
        G_number_of_cycle_per_second : integer := 100000000);  -- number of clock cycles in one second with 100MHz clock
    port (
        I_clk            : in  std_logic;                      -- global clock
        I_rst            : in  std_logic;                      -- global asynchronous reset
        I_presence       : in  std_logic;                      -- Presence button
        O_led            : out std_logic_vector(5 downto 0);   -- Led vector RV & RO & RR & CV & CO & CR
        O_7segment       : out std_logic_vector(6 downto 0);   -- 7 segment representation of decimal digit
        O_7segmentSelect : out std_logic_vector(7 downto 0)    -- selects one among 8
        );
end entity carrefourGlobal;

architecture archi_carrefourGlobal of carrefourGlobal is

    component carrefourUnit is
        generic (
            G_number_of_cycle_per_second : integer
            );
        port (
            I_clk      : in  std_logic;
            I_rst      : in  std_logic;
            I_presence : in  std_logic;
            O_led      : out std_logic_vector(5 downto 0);
            O_dataOut  : out std_logic_vector(3 downto 0));
    end component carrefourUnit;

    component decimalTo7segment is
        port (
            I_dataIn   : in  std_logic_vector(3 downto 0);
            O_7segment : out std_logic_vector(6 downto 0));
    end component decimalTo7segment;

    signal SC_counterOut : std_logic_vector(3 downto 0);

begin

    carrefourUnit_1 : carrefourUnit
        generic map (
            G_number_of_cycle_per_second => G_number_of_cycle_per_second
            )
        port map (
            I_clk      => I_clk,
            I_rst      => I_rst,
            I_presence => I_presence,
            O_led      => O_led,
            O_dataOut  => SC_counterOut);

    decimalTo7segment_1 : decimalTo7segment
        port map (
            I_dataIn   => SC_counterOut,
            O_7segment => O_7segment);

    O_7segmentSelect <= "11111110";     -- selection of the last 7 segment display, active low

end architecture archi_carrefourGlobal;
