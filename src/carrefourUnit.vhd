-------------------------------------------------------------------------------
-- Title      : carrefourUnit
-- Project    :
-------------------------------------------------------------------------------
-- File       : carrefourUnit.vhd
-- Author     : Jean-Noel BAZIN  <jnbazin@pc-disi-026.enst-bretagne.fr>
-- Company    :
-- Created    : 2018-05-30
-- Last update: 2024-03-27
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Connection between the counter and the FSM
-------------------------------------------------------------------------------
-- Copyright (c) 2018
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-05-30  1.0      jnbazin Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity carrefourUnit is

  generic (
    G_number_of_cycle_per_second : integer := 2  -- number of clock cycles in one second, small value for test,  50 000 000 for 50MHz clock onboard
    );

  port (
    I_clk      : in  std_logic;                      -- Global clock
    I_rst      : in  std_logic;                      -- Global asynchronous active high reset
    I_presence : in  std_logic;                      -- A vehicule is present on the track
    O_led      : out std_logic_vector(5 downto 0);   -- drive the leds of the traffic light the signal has the format  :  RV & RO & RR & CV & CO & CR
    O_dataOut  : out std_logic_vector(3 downto 0));  -- Output counter value

end entity carrefourUnit;

architecture archi_carrefourUnit of carrefourUnit is
  --component fsmMealy is
  --  port (
  --    I_clk         : in  std_logic;
  --    I_rst         : in  std_logic;
  --    I_timerDone   : in  std_logic;
  --    I_presence    : in  std_logic;
  --    O_led         : out std_logic_vector(5 downto 0);
  --    O_maxCount    : out std_logic_vector(3 downto 0);
  --    O_initTimer   : out std_logic;
  --    O_enableTimer : out std_logic);
  --end component fsmMealy;

  component fsmMoore is
    port (
      I_clk         : in  std_logic;
      I_rst         : in  std_logic;
      I_timerDone   : in  std_logic;
      I_presence    : in  std_logic;
      O_led         : out std_logic_vector(5 downto 0);
      O_maxCount    : out std_logic_vector(3 downto 0);
      O_initTimer   : out std_logic;
      O_enableTimer : out std_logic);
  end component fsmMoore;

  component counter is
    generic (
      G_number_of_cycle_per_second : integer := 1  -- number of clock cycle in
      );                                           -- one second
    port (
      I_clk      : in  std_logic;
      I_init     : in  std_logic;
      I_enable   : in  std_logic;
      I_maxCount : in  std_logic_vector(3 downto 0);
      O_dataOut  : out std_logic_vector(3 downto 0);
      O_done     : out std_logic);
  end component counter;

  signal SC_timerDone   : std_logic;
  signal SC_maxCount    : std_logic_vector(3 downto 0);
  signal SC_initTimer   : std_logic;
  signal SC_enableTimer : std_logic;

begin  -- architecture archi_carrefourUnit

  fsm_1 : fsmMoore __BLANK_TO_FILL__
    __BLANK_TO_FILL__


  counter_1 : counter __BLANK_TO_FILL__
    __BLANK_TO_FILL__

end architecture archi_carrefourUnit;
