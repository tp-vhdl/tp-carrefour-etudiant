-------------------------------------------------------------------------------
-- Title      :
-- Project    :
-------------------------------------------------------------------------------
-- File       : carrefourUnit.vhd
-- Author     : Jean-Noel BAZIN  <jnbazin@pc-disi-026.enst-bretagne.fr>
-- Company    :
-- Created    : 2018-05-30
-- Last update: 2019-10-16
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: converts the 4-bit representation of a decimal (I_dataIn) into a 7-segment display command (O_7segment). 
-- A display command is active low:  to turn on a segment, apply 0. 
-- a (= O_7segment(6)) is the MSB, g (=O_7segment(0)) is the LSB.
-- So, for digit 3 the output will be represented by "0000110".
--      _______
--     |   a   |
--    f|       |b
--     |       |
--     |_______|
--     |   g   |
--    e|       |c
--     |       |
--     |_______|
--         d
--
-------------------------------------------------------------------------------
-- Copyright (c) 2018
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-05-30  1.0      jnbazin Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity decimalTo7segment is
  port(
    I_dataIn   : in  std_logic_vector(3 downto 0);  -- four bit representation of a decimal digit
    O_7segment : out std_logic_vector(6 downto 0)   -- 7 segment representation of the input
    );
end entity decimalTo7segment;

architecture archi_decimalTo7segment of decimalTo7segment is

begin  -- architecture archi_decimalTo7segment

  process(I_dataIn)
  begin
    case I_dataIn is
      when __BLANK_TO_FILL__
        __BLANK_TO_FILL__
    end case;
  end process;

end architecture archi_decimalTo7segment;
