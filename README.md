# TP Carrefour

Énoncé du TP : [https://mee-labs.gitlab-pages.imt-atlantique.fr/vhdl/carrefour/](https://mee-labs.gitlab-pages.imt-atlantique.fr/vhdl/carrefour/)

## Questions

Les réponses aux questions du sujet doivent être complétées dans le fichier markdown `doc/compte-rendu.md` et le diagramme de la FSM `doc/img/FSM-moore.drawio` modifier grâce à l'outil en ligne [https://app.diagrams.net/](Draw.io), en choisissant l'enregistrement sur périphérique. Il faut ensuite l'exporter au format PNG en écrasant le fichier PNG existant.
