---
title: "Compte rendu TP carrefour"
author: John Doe
geometry: margin=1cm
output: pdf_document
mainfont: sans-serif
---





# Compte rendu du TP Carrefour

## Diagramme de la FSM

![Diagramme de la FSM](./img/FSM-moore.png)


## Questions

### Question 1 : Combien de LUT sont utilisées ?

### Question 2 : Combien de Flip Flop sont utilisées, est-ce cohérent avec le VHDL ?

### Question 3 : Combien de latchs sont utilisées ? Pourquoi ?
